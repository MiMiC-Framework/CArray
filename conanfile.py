from conans import ConanFile, CMake, tools
import os

class CArrayConan(ConanFile):
    name = "CArray"
    version = "0.1"
    license = "Raising The Floor"
    url = "https://gitlab.com/MiMiC-Framework/CArray"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "cmake"
    requires = (
        "gtest/1.8.0@lasote/stable",
        "CVariant/0.1@javjarfer/testing",
        "UErrno/0.1@javjarfer/testing"
    )
    default_options = (
        "shared=False",
        "tests=False",
        #  GTest options
        "gtest:shared=False",
        "gtest:no_gmock=True",
        "gtest:no_main=True"
    )

    def source(self):
        self.run("git clone https://gitlab.com/MiMiC-Framework/CArray")
        self.run("cd CArray && git checkout develop")

    def build(self):
        cmake = CMake(self, parallel=True)
        if self.options.tests:
            cmake.definitions["CMAKE_BUILD_TESTS"] = "ON"
        cmake.configure(source_folder="CArray")
        cmake.build(target="install")

    def package(self):
        self.copy("*.h", dst="include/CArray", src="include")
        self.copy("*CArray.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["CArray"]
