#ifndef CARRAY_H
#define CARRAY_H

#include <stdint.h>
#include <CVariant/Variant.h>
#include <UErrno/uerrno.h>

extern "C" {

struct CArray {
    void*   data;
    size_t  size;
    size_t  offset;
};

struct ArrayDim {
    size_t size;
    size_t offset;
};

struct NArray {
    void*       data;
    size_t      dimN;
    ArrayDim*   dims;
};

struct GArray {
    void*       data;
    size_t      size;
    size_t      offset;
    VariantType type;
};

struct NGArray {
    void*       data;
    size_t      dimN;
    ArrayDim*   dims;
    VariantType type;
};

errno_t initArray(size_t elems, size_t typeSize, struct CArray* arr);
errno_t initNArray(size_t elems, size_t dims, size_t typeSize, struct NArray* arr);

}

#endif
