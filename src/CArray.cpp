#include <CArray/Array.h>
#include <stdlib.h>

errno_t initArray(size_t elems, size_t typeSize, struct CArray* arr) {
    errno = 0;

    void* data = (void*)malloc(elems*sizeof(typeSize));

    if (data == NULL) {
        return errno;
    }

    arr->data = data;
    arr->size = elems;
    arr->offset = 0;

    return 0;
}

errno_t initNArray(size_t elems, size_t dimN, size_t typeSize, struct NArray* arr) {
    errno = 0;

    ArrayDim* dims = (ArrayDim*)malloc(dimN*sizeof(ArrayDim));
    if (dims == NULL) {
        return errno;
    }

    for (int i = 0; i < dimN; i++) {
        (dims + i)->size = elems;
        (dims + i)->offset = 0;
    }

    void* data = (void*)malloc(elems*dimN*sizeof(typeSize));
    if (data == NULL) {
        return errno;
    }

    arr->data = data;
    arr->dims = dims;

    return 0;
}
